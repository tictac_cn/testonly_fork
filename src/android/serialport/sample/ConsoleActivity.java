/*
 * Copyright 2009 Cedric Priscal
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package android.serialport.sample;

import java.io.IOException;

import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class ConsoleActivity extends SerialPortActivity
{

	EditText mReception;
	private int oldx = 0;
	private int oldy = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.console);

		// setTitle("Loopback test");
		mReception = (EditText) findViewById(R.id.EditTextReception);

		EditText Emission = (EditText) findViewById(R.id.EditTextEmission);
		Emission.setOnEditorActionListener(new OnEditorActionListener()
		{
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				int i;
				CharSequence t = v.getText();
				char[] text = new char[t.length()];
				for (i = 0; i < t.length(); i++)
				{
					text[i] = t.charAt(i);
				}
				try
				{
					mOutputStream.write(new String(text).getBytes());
					mOutputStream.write('\n');
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
				return false;
			}
		});
	}

	@Override
	protected void onDataReceived(final byte[] buffer, final int size)
	{
		runOnUiThread(new Runnable()
		{
			public void run()
			{
				if (mReception != null)
				{
					mReception.append(Integer.toString(size));
					mReception.append("<<<");
					int press = 0;
					if(size % 6 == 0)
					{
						press = buffer[1];
						if(press == 0x81)
						{
							for (int i = 0; i < 6; i++)
							{
								mReception.append(Integer.toHexString(buffer[i]));
								mReception.append(".");
							}
						}
					}
//					for (int i = 0; i < size; i++)
//					{
//						int ready = buffer[i] & 0xFF;
//						if(ready == 0xFC || ready == 0x03 || ready == 0xA0 || ready == 0xA0 || ready == 0xFD || ready == 0x80)
//						{
//							mReception.append(Integer.toHexString(buffer[i]));
//							mReception.append("/");
//						}
//						mReception.append(Integer.toHexString(buffer[i]));
//						mReception.append(".");
//					}
//					byte Xlow = buffer[2];
//					byte Xhigh = buffer[3];
//					byte Ylow = buffer[4];
//					byte Yhigh = buffer[5];
//					int state = buffer[0];
//					int color = buffer[1];
//					int x = (Xhigh << 8) | Xlow;
//					int y = (Yhigh << 8) | Ylow;
//					if(Math.abs(x - oldx) > 100 || Math.abs(y - oldy) > 100)
//					{
//						oldx = x;
//						oldy = y;
//						mReception.append("(");
//						mReception.append(Integer.toString(state));
//						mReception.append(",");
//						mReception.append(Integer.toString(color));
//						mReception.append(",");
//						mReception.append(Integer.toString(x));
//						mReception.append(",");
//						mReception.append(Integer.toString(y));
//						mReception.append(")");
//						mReception.append("<<<");
//					}
					//mReception.append(new String(buffer, 0, size));
				}
			}
		});
	}
}
